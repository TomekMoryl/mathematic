package com.github.nyholmniklas.arithmetic;

import java.math.BigDecimal;

/**
 * Really silly class that provides the same functionality as {@link BigDecimal}
 * itself for simple arithmetic operationsXD
 */
public class NewOperation
{
    public static BigDecimal add(BigDecimal x, BigDecimal y, BigDecimal z) {
        return x.add(y.add(z));
    }
}
